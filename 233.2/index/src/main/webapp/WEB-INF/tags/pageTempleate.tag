<%-- 
    Document   : page
    Created on : Nov 8, 2018, 5:22:15 PM
    Author     : 233-2
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message"%>

<%-- any content can be specified here e.g.: --%>
<html> 
    <head>
        <title>${pageTitle}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-5">
    </head>
    <body>
        <jsp:doBody />
    </body>
    </head></html>